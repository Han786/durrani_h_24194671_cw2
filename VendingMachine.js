//prepares the code for the user and then allows the user to answer any of the inputs e.g.
//please enter the number of the option you would like to choose.
readline = require('readline-sync');

//a variable called colours is created which allows the use of changing
//certain lines of code to different colours
var colors = require('colors');

var balance = 0; //creates a variable called balance and sets it to be at 0 from the start

function main_menu() { //a main menu function is created that is the first thing the user sees
  console.log('+============================+'.rainbow); //this line will be outputted as a rainbow colour
  console.log('|Welcome to the vending machine|'.white);//this line will be outputted in the colour white
  console.log('|Choose from the options below to decide what you would like to do!|'.white);//this will output in the colour white
  console.log('+============================+'.rainbow);//this line will be outputted as a rainbow colour
}

//This item function holds all the objects
function item() {
  this.id = ''; //sets the id as being a string
  this.name = ''; //sets the name as being a string
  this.price = 0.00; //sets the price as being a floating point number
}

//The lines of code below are all the objects that are used in the vending machine.
//Each item has an ID number, the name of the item and the price
var A1 = new item();
A1.id = 'A1';
A1.name = 'Prawn Cocktail Crisps';
A1.price = 1.20;

var A2 = new item();
A2.id = 'A2';
A2.name = 'Salt and Vinegar Crisps';
A2.price = 1.20;
A2.stock = 5;

var A3 = new item();
A3.id = 'A3';
A3.name = 'Dairy Milk';
A3.price = 0.60;

var A4 = new item();
A4.id = 'A4';
A4.name = 'Crunchie';
A4.price = 0.75;

var A5 = new item;
A5.id = 'A5';
A5.name = 'Coke';
A5.price = 1.50;

var A6 = new item;
A6.id = 'A6';
A6.name = 'Fanta Orange';
A6.price = 1.45;

var A7 = new item;
A7.id = 'A7';
A7.name = 'Water';
A7.price = 0.90;

//This function shows the options you can choose from to do something
function options() {
  console.log('1. View the available items');
  console.log('2. Add money to your balance');
  console.log('3. Remove money from your balance');
  console.log('4. View your current balance');
  console.log('5. Purchase an item');
  console.log('6. Exit the vending machine');

  //This line enables the user to enter the number of the option they want to choose
  var input = readline.question("\n" + 'Please enter the number of the option you would like to choose: '.underline.bold.red);

  if (input == 1) { //if the user enters the number 1
    console.log("\n" + 'You have selected Option 1: View the available items'.bold.white); //this text will be outputted
    listOfItems(); //the list of items available to choose from will be outputted
  } else if(input == 2)  {  //but if the user enters the number 2
    console.log("\n" + 'You have selected Option 2: Add money to your balance'.bold.white); //this text will be outputted
    addMoney(); //the user will now be able to add money to their balance
  } else if(input == 3) { //if the user enters the number 3
    console.log("\n" + 'You have selected Option 3: Remove money from your balance'.bold.white); //this text will be outputted
    removeMoney();  //the user will now be able to remove the money that's in their balance
  } else if(input == 4) { //if the user enters the number 4
    console.log("\n" + 'You have selected Option 4: View your current balance'.bold.white); //this text will be outputted
    viewBalance();  //the user will now be able to view their current total balance
  } else if(input == 5) { //if the user enters the number 5
    console.log("\n" + 'You have selected Option 5: Purchase an item'.bold.white); //this text will be outputted
    purchase();
  } else if(input == 6) {  //if the user enters the number 6
    console.log('+=====================================================================+'.rainbow);
    console.log(  'Thank you for using this vending machine. We hope to see you again!'.white); //this message will be outputted
    console.log('+=====================================================================+'.rainbow);
  }
  else {  //if the user enters something that isn't one of the relevant numbers
    console.log("\n" + 'That was not an option, please select a valid number again'.bold.white);//this message will be outputted
    options();  //the user will now be able to see the list of items available to choose from again
  }


//This function is created to hold the the list of items that are available in the vending machine
function listOfItems() {
  console.log('The items that are available are: ' + "\n");
  console.log(A1) //outputs the ID, name and price of A1
  console.log(A2) //outputs the ID, name and price of A2
  console.log(A3) //outputs the ID, name and price of A3
  console.log(A4) //outputs the ID, name and price of A4
  console.log(A5) //outputs the ID, name and price of A5
  console.log(A6) //outputs the ID, name and price of A6
  console.log(A7) //outputs the ID, name and price of A7
  console.log("\n") //an empty new line is created to space things out
  options(); //the user will now be able to see the list of items available to choose from again

}

//This function is created to allow users to add money to their balance
function addMoney() {
  console.log('To add more money to your balance please follow the instructions below'.white); //this line is outputted
  var credit = readline.question("Enter the amount: ".bold.red); //this line is outputted and allows the user to add money
  //the line below takes the balance and adds the amount the user entered to it
  balance = parseFloat(credit) + balance; //the parseFloat takes in a string value and converts it to a floating point number
  //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
  console.log('Your new balance is: ' + '£' + balance.toFixed(2).white); //hence the 2 in the brackets
  console.log("\n"); // an empty new line is created to space things out
  options();
  return balance;
}

//This function is created to allow users to remove their balance
function removeMoney() {
  balance = 0; //sets the balance to be 0 again if the user selects this option
  //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
  console.log('Your new total balance is: ' + '£' + balance.toFixed(2).white); //hence the 2 in the brackets
  console.log("\n"); //an empty new line is created to space things out
  options();
  return balance; //the balance is returned
}

//This function is created to allow users to view their total balance
function viewBalance() {
  //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
  console.log('Your current balance is' + '£' + balance.toFixed(2).white); //hence the 2 in the brackets
  console.log("\n"); //a new line is created to space things out
  options();
}

//This function is created to allow users to purchase an item of their choice
function purchase() {
  //a variable called choose is created and the readline.question is used to allow the user to answer the question below
  var choose = readline.question('Please enter the ID number of the item that you would like to buy'.underline.red);
  if (choose == A1.id) { //if the user chooses A1
    console.log('You have selected: ' + A1.name.green); //this line is outputted and the name in a green colour
    if (balance >= A1.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A1.price; //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item'+ "\n" + 'Your new balance after the purhase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A2.id) { //if the user instead chooses A2
  console.log('You have selected: ' + A2.name.green); //this line is outputted and the name in a green colour
  if (balance >= A2.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A2.price; //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A3.id) { //if the user instead chooses A3
  console.log('You have selected: ' + A3.name.green); //this line is outputted and the name in a green colour
  if (balance >= A3.price) {  //if the balance is greater than or equal to the price of the item
    balance = balance - A3.price; //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A4.id) { //if the user instead chooses A4
  console.log('You have selected: ' + A4.name.green); //this line is outputted and the name in a green colour
  if (balance >= A4.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A4.price; //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A5.id) { //if the user instead chooses A5
  console.log('You have selected: ' + A5.name.green); //this line is outputted and the name in a green colour
  if (balance >= A5.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A5.price; //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A6.id) { //if the user instead chooses A6
  console.log('You have seleted: ' + A6.name.green); //this line is outputted and the name in a green colour
  if (balance >= A6.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A6.price;  //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance;  //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else if (choose == A7.id) { //if the user instead chooses A6
  console.log('You have selected ' + A7.name.green); //this line is outputted and the name in a green colour
  if (balance >= A7.price) { //if the balance is greater than or equal to the price of the item
    balance = balance - A7.price;  //then the item price can be deducted from the balance
    console.log('Thank you for purchasing an item' + "\n" + 'Your new balance after the purchase is '.green + balance);
    options();
    return balance; //the balance is returned
  } else {
    //the balance.toFixed(2) below takes an object number and turns it into a string version which is rounded to two decimal places
    console.log('You do not have enough money, your balance is' + '£' + balance.toFixed(2)); //hence the 2 in the brackets
    options();
  }

} else { //if the user enters something that isn't one of the relevant ID's
  //the line below will output
  console.log('The number that you input does not match any of our current products. Please check the ID number again and select a relevant one.');
  listOfItems(); //allows the user to enter a relevant number again
  select();
  }
}
}
main_menu();
options();
